import 'package:flutter/material.dart';
import 'dart:developer';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final id = TextEditingController();
    final name = TextEditingController();
    final lastposition = TextEditingController();
    final email = TextEditingController();
    final linkedin = TextEditingController();
    final github = TextEditingController();
    final status = TextEditingController();

    // Método responsável por limpar os dados
    void clear(){
      id.clear();
      name.clear();
      lastposition.clear();
      email.clear();
      linkedin.clear();
      github.clear();
      status.clear();
    }

    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        backgroundColor: Colors.lightBlue[300],  
        appBar: AppBar(
          title: Image.asset('imagens/mvpFlutter.png'),
          backgroundColor: Colors.black87,
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildTextField("Id", "prefix",id),
              Divider(),
              buildTextField("Nome", "prefix",name),
              Divider(),
              buildTextField("Último cargo", "prefix",lastposition),
              Divider(),
              buildTextField("Email", "prefix",email),
              Divider(),
              buildTextField("Linkedin", "prefix",linkedin),
              Divider(),
              buildTextField("GitHub", "prefix",github),
              Divider(),
              buildTextField("Status", "prefix",status),
              Divider(),

              //Logs mostram os controllers com os respectivos dados
              FloatingActionButton.extended(onPressed: (){
                log('Id = '+id.text);
                log('Name = '+name.text);
                log('LastPosition = '+lastposition.text);
                log('Email = '+email.text);
                log('Linkedin = '+linkedin.text);
                log('GitHub = '+github.text);
                log('Status = '+status.text);

                // Método responsável por limpar os dados dos controllers
                clear();
              },
              tooltip: "Enviar para o banco",
              label: Text('Enviar'),
              backgroundColor: Colors.amber,              
              ),              
            ],

          ),
        ),
      ),
    );
  }
}

Widget buildTextField(String label, String prefix,TextEditingController c){
  return TextField(
    controller: c,
    decoration: InputDecoration(
      labelText: label,
      labelStyle: TextStyle(color: Colors.white60),
      border: OutlineInputBorder(),
    ),
    style: TextStyle(
      fontSize: 15.0
    ),
 
  );
}